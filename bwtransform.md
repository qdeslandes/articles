Compression par redondance et transformée de Burrows-Wheeler
============================================================

 
# Un peu de théorie...  
La compression est une technique largement utilisée par les ordinateurs à travers le monde afin de réduire la taille des fichiers. Que ce soit de la compression audio (MP3), d'images (JPEG), vidéo...  

Le principe de la compression est relativement simple, pour réduire la taille de certains fichiers, les données jugées non nécessaires sont retirées pour en diminuer la taille. Le retrait de ces données peut se faire de deux manières différentes,
soit sans perte, soit avec (en toute logique). Lors d'une compression sans
perte, il est toujours possible de récupérer les données supprimées grâce aux
algorithmes utilisés. Dans le cas d'une compression avec perte, cela n'est pas
possible. Parmi ces algorithmes de compression avec perte, on notera JPEG et MP3
comme faisant partie des plus connus, mais il en existe beaucoup d'autres.

Aujourd'hui, ce sont les algorithmes de compression sans perte qui nous
intéressent, notamment la **compression par redondance**. C'est cette méthode qui est utilisé dans l'algorithme bzip2, ce dernier devrait parler
aux utilisateurs Linux. Le principe de la compression par redondance est assez
simpliste mais plutôt efficace. Prenons un texte d'entrée aléatoire :

>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AZZAAADFVVVVVDDEEERRRRLELLLL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Bon, il est vrai qu'il n'est que peu aléatoire, mais il faut bien servir mon
propos ;)
Donc selon le principe de compression par redondance (dans les grandes lignes),
nous remplaçons chaque séquence de plus de 1 lettre identique par le couple
`[nombre_répétitions, lettre]`. Ainsi, 'A' ne changera pas, 'ZZ' sera
remplacé par '2Z', 'AAA' par '3A', etc... Notre séquence d'origine sera donc :

>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1A2Z3A1D1F5V2D3E4R1L1E3L
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On remarque alors que le texte "compressé" est plus court, notre algorithme est
efficace, hourra !

##### _HAHA ! Tu as tout calculé ! Ton texte avait plein de répétitions !_

Il est vrai, le texte d'entrée comptait énormément de séquences de
lettres successives, il était donc évident que la compression serait bénéfique.
Si l'on prend un texte cette fois ci \*totalement\* aléatoire :

>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Peut-être suis-je vieux et fatigué mais je crois que les chances de savoir ce qui se passe réellement sont si ridiculement minces que la seule chose à faire c'est de renoncer à chercher et de chercher à s'occuper. Je préfère mille fois être heureux qu'être dans le vrai.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les séquences de lettres identiques se font bien évidement plus rares... Ainsi,
en ajoutant sur chaque lettre le nombre de fois où elle apparaît de manière
successive, on en viendrait presque à préfixer chaque lettre avec '1', ce qui
serait tout à fait contre-productif...

##### _Donc ton algorithme de compression augmente la taille de mes fichiers ?_

Non, évidement, seulement la compression par redondance n'est pas uniquement
constituée du mécanisme que nous venons de voir. En effet, si l'on reprends
bzip2 vu juste avant, on remarque sur
[Wikipédia](<https://fr.wikipedia.org/wiki/Bzip2>) que celui-ci utilise la
transformée de **Burrows-Wheeler** !

Qu'est-ce donc que la transformée de Burrows-Wheeler ? C'est tout simplement un
algorithme permettant, dans un texte donné, de "rapprocher" les lettres
identiques, pour ensuite pouvoir effectuer une compression plus efficace. Pour
simplifier les choses, prenons un exemple concret et tâchons de transformer le
texte suivant : `voyageur galactique` (le hasard fait décidément bien les choses).

Il faut tout d'abord commencer par lister toutes les rotations possibles du
texte, i.e. tous les textes différentes que nous pouvons obtenir en décalant les
lettres, et trions les dans l'ordre alphabétique
(l'espace est à considérer comme une lettre, il sera placé au début du tri dans
l'ordre alphabétique, comme si nous trions à l'aide d'une table ASCII). A savoir :

>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Rotations              Tri
voyageur galactique     galactiquevoyageur
oyageur galactiquev    actiquevoyageur gal
yageur galactiquevo    ageur galactiquevoy
ageur galactiquevoy    alactiquevoyageur g
geur galactiquevoya    ctiquevoyageur gala
eur galactiquevoyag    eur galactiquevoyag
ur galactiquevoyage    evoyageur galactiqu
r galactiquevoyageu    galactiquevoyageur
 galactiquevoyageur    geur galactiquevoya
galactiquevoyageur     iquevoyageur galact
alactiquevoyageur g    lactiquevoyageur ga
lactiquevoyageur ga    oyageur galactiquev
actiquevoyageur gal    quevoyageur galacti
ctiquevoyageur gala    r galactiquevoyageu
tiquevoyageur galac    tiquevoyageur galac
iquevoyageur galact    uevoyageur galactiq
quevoyageur galacti    ur galactiquevoyage
uevoyageur galactiq    voyageur galactique
evoyageur galactiqu    yageur galactiquevo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On peut alors récupérer la dernière lettre de chaque rotations afin de
former la phrase transformée. Notre phrase transformée sera donc.... `rlygagu_ataviucqeeo` ! Pour finir la transformation, nous devons ajouter, à la fin de la chaine, la **clé de la transformation**. La clé de la transformation est l'index de la première lettre du texte original par rapport à la chaine transformée. Pour nous, la première du texte original est la lettre 'v', elle se trouve en 11ème position de la chaine triée (nous commençons à 0), notre clé est donc 11. La chaine transformée finale est donc `rlygagu ataviucceeo11`. Par ailleurs, s'il se trouvait que notre texte comportait plusieurs fois la lettre 'v', nous aurions dû prendre celle correspondante **réellement** à la première lettre de notre texte, à savoir celle correspondant à la rotation d'index 0 ! L'efficacité n'est pas flagrante, soit, mais le texte ici était relativement court, plus le texte sera long, plus cette
méthode sera efficace !

##### _Bien, mais mon texte, je le récupère comment ?_ 

Pour l'effet inverse, la procédure à suivre est différente. On commence par
trier les lettres du mot transformé par ordre lexicographique croissant, notre
texte devient alors `_aaaceeggiloqrtuuvy` (l'underscore _ représentant le caractère espace).

De fait, on peut alors représenter de la manière suivante le texte transformé et le mot formé des lettres triées :

>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
r | l | y | g | a | g | u | _ | a | t | a | v | i | u | c | q | e | e | o | 11
_ | a | a | a | c | e | e | g | g | i | l | o | q | r | t | u | u | v | y
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A l'aide de ce tableau, nous pouvons alors retrouver la chaîne originelle. Pour cela, on associe à chaque caractère de la première ligne, le caractère de même index sur la seconde ligne (concrètement, les lettres sont associées par colonnes). Ensuite, nous pouvons construire le tableau _indices_, tel que _indices[i]_ soit l'indice de la lettre _tableau_trié[i]_ dans le texte transformé. Si plusieurs occurrences de cette lettre apparaissent dans la chaîne transformée, on fait correspondre celle qui figure au même rang \*relatif\* dans la chaîne transformée, à savoir, le premier 'a' de la chaîne triée correspondra au premier 'a' trouvé dans la chaîne transformée ! En ce qui nous concerne, notre tableau d'indices est le suivant :

>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
r | l | y | g | a | g | u | _ | a | t | a | v | i | u | c | q | e | e | o | 11
_ | a | a | a | c | e | e | g | g | i | l | o | q | r | t | u | u | v | y
7 | 4 | 8 | 10| 14| 16| 17| 3 | 5 | 12| 1 | 18| 15| 0 | 9 | 6 | 13| 11| 2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Enfin, en partant de la première lettre du texte d'origine (celle représentée par la clé de la transformation), nous pouvons retrouver le texte complet, en associant à chaque lettre _texte_transformé[i]_ l'entier correspondant à la lettre suivante dans le texte d'origine présent à _indices[i]_ ! Un exemple ne serait pas superflu :

>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
r | l | y | g | a | g | u | _ | a | t | a | v | i | u | c | q | e | e | o | 11
7 | 4 | 8 | 10| 14| 16| 17| 3 | 5 | 12| 1 | 18| 15| 0 | 9 | 6 | 13| 11| 2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ici, nous avons supprimé le tableau de caractères triés, nous n'en avons plus besoin.

Nous partons de :  
>`texte_transformé[11] = c (première lettre du texte d'origine)`

Nous lui assoçions donc :
>`indices[11] = 18`

La seconde lettre du texte sera alors la 18ème lettre du texte transformé !
Ainsi,  
>`texte_transformé[18] = o (seconde lettre du texte d'origine)`

Nous lui associons :  
>`indices[18] = 2`

La troisième lettre du texte sera alors la 2nde lettre du texte transformé, ainsi de suite...

# Place à la pratique

Maintenant que nous voyons globalement comment tout cela fonctionne, il ne reste plus qu'à en coder une implémentation !

## La compression par redondance

Tout d'abord, une petite précision concernant la compression par redondance, disons qu'elle n'est pas tout à fait telle que décrite plus haut. En réalité, c'est un peu moins simple (mais pas plus compliqué pour autant). Il faut tout d'abord trouver le **marqueur**, le marqueur est le plus petit caractère le moins présent dans le texte d'origine. Ensuite, lorsque nous compresserons le texte, nous placerons le marqueur au début du texte compressé. Lorsque l'algorithme analysera le texte, deux cas de figure peuvent survenir :

* Plusieurs caractères identiques les uns à la suite des autres ("aa", "tttt", ...), dans ce cas, nous remplacerons ces caractères par le triplet :  
  `[marqueur, nombre de caractères identiques successifs, caractère]`

* Un caractère seul, pas de série :
  Nous laisserons ce caractère intact

Effectuer la compression par redondance va nécessiter différentes étapes, tout d'abord trouver le marqueur, ensuite, on prend le temps de calculer la future taille du texte compressé, puis on compresse ledit texte.

##### Trouvons le marqueur

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ c
/* occurences: compute occurences of each letter in t */
int *occurences(const char *t, int n)
{
        int *r = malloc(128 * sizeof(int));

        if (! r) {
                printf("ERROR : Could not alloc memory\n");
                return NULL;
        }

        for (int i = n-1; i--; ) {
                ++r[(int)t[i]];
        }

        return r;
}

/* compute_marker: compute the marker of the t array */
int compute_marker(const char *t, int n)
{
        int *r = occurences(t, n);

        if (! r) {
                printf("ERROR : No occurence array\n");
                return NULL;
        }

        int min = 0;

        for (int i = 1; i < 128; ++i) {
                if (r[i] < r[min])
                        min = i;
        }

        free(r);
        return min;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nous avons ici deux fonctions, l'une, `int *occurences(const char *t, int n)` qui, à partir du texte `t` et de sa taille `n` nous retourne un pointeur vers un tableau comptant le nombre d'occurrences de chaque lettres dans le texte. Il ne faut pas oublier de vérifier que [`calloc()`](http://www.cplusplus.com/reference/cstdlib/calloc/) nous retourne bel et bien un pointeur vers une zone mémoire allouée, et non `NULL` car une erreur s'est produite ! Ici, il est important que chaque octet de notre tableau d'occurrences soit définit à zéro, ainsi par le suite, nous n'aurons qu'a incrémenter l'index nécessaire. C'est pour cette raison que nous préférerons `calloc()` à `malloc()`.

Ensuite, nous bouclons de `n-1` (la dernière lettre du texte) à 0 grâce à une boucle `for`. La condition de cette dernière est un peu particulière, étant donné que l'affectation est effectuée en même temps. Lorsque l'expression `i--` sera évaluée, `i--` retournera la valeur actuelle de `i` (grâce à l'opérateur `--` suffixé) puis `i` sera décrémenté. Ainsi, lorsque `i` vaudra 0 **dans la boucle**, au tour suivant `i--` retournera 0, la condition sera fausse, la boucle s'arrêtera.

On peut alors incrémenter, dans le tableau `r` des occurrences, la lettre possédant le caractère ASCII numéro `t[i]`. Enfin, on retourne le pointeur vers le tableau d'occurrences.

Ce tableau d'occurrences est retournée à `int compute_marker(const char *t, int n)`. Cette fonction se chargera alors de parcourir chaque case du tableau d'occurrences, et ainsi découvrir quelle est la plus petite lettre apparaissant le moins souvent. On oubliera pas ensuite de `free()` la mémoire allouée, et de retourner le marqueur.

##### Le marqueur, c'est bon ! Maintenant il nous faut calculer la taille du texte compressé !

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ c
/* compressed_size: return the compressed size of the text */
int compressed_size(const char *t, int n)
{
        int size = 1; /* 1 for the marker */

        for (int i = 0; i < n-1; i++) {
                int tmp = i;
                if (i+1 < n && t[i] == t[i+1])
                        ++i;

                if (tmp == i)
                        size += 1;
                else
                        size += 3;
        }

        return size;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La taille de départ du texte compressé est forcémment 1, car l'emplacement correspond au marqueur. Ensuite, nous bouclons de 0 à taille_du_tableau - 1. A chaque tour de boucle, nous vérifions la condition `i+1 < n && t[i] == t[i+1]`, à savoir, vérifier que `i + 1` est bien un index du tableau, puis si `t[i] == t[i+1]`, auquel cas nous avons à faire à une succession de lettres identiques. Ici, l'ordre de l'expression est primordial. En effet, le Standard spécifie que, lors de la vérification d'une expression composée elle-même d'expression (ici `(expression) && (expression)`), l'analyse s'arrête dès que le résultat final est connu du programme. Pour nous, si `i+1 < n` est faux, l'expression entière aura également pour valeur faux. Ainsi, si l'index `i+1` n'est pas un index du tableau, le programme ne tentera pas d'accéder à `t[i+1]` dans l'expression suivante. Si nous avions fait les choses dans l'ordre inverse et que l'index `i+1` ne faisait pas partie du tableau, nous aurions eu à faire à un _segfault_ (en fait non, mais ça ira comme ça pour le moment, c.f. note en bas de page).

##### Maintenant, on compresse !

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ c
/* compress: compress t string by redundancy */
char *compress(const char *t, int n, int *rsize)
{
        int marker = compute_marker(t, n);

        *rsize = compressed_size(t, n);
        char *r = malloc((*rsize) * sizeof(char));

        if (! r) {
                printf("ERROR : Coud not alloc memory for commpressed string\n");
                return NULL;
        }

        r[0] = marker;
        int ri = 1;
        for (int i = 0; i < n; ++i) {
                int tmp = i;

                while (i+1 < n && t[i] == t[i+1])
                        ++i;

                if (tmp == i) {
                        r[ri] = t[i];
                        ++ri;
                } else {
                        r[ri] = marker;
                        r[ri+1] = (i - tmp) + 1;
                        r[ri+2] = t[i];
                        ri += 3;
                }
        }

        return r;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour commencer, on récupère le marqueur ainsi que la taille de la chaîne compressée. Ensuite, on créer un tableau de caractères de la taille de la chaîne compressée, on peut alors placer le marqueur à l'index 0 de cette chaîne. On définit ensuite `ri` qui représente l'index où nous pouvons écrire dans la chaîne compressée.

Viens alors la boucle for qui parcours tous les caractères de la chaîne d'origine, pour chaque caractère, on mémorise son index (`int tmp = i;`), on parcourt tous les caractères successifs identiques (si besoin est) et on ajoute le texte compressé correspondant (comme présenté au début de la partie "Pratique").

## Transformée de Burrows-Wheeler

Nous sommes désormais capables de compresser notre texte. Mais la compression n'est rien si les caractères identiques ne sont pas successifs ! Ainsi, nous allons appliquer la transformée de Burrows-Wheeler (au passage, la transformée s'applique au texte _avant_ la compression, et non après, j'aime être imprévisible ;)).

Pour effectuer la transformation, nous devons également passer par plusieurs étapes. Tout d'abord, trier les rotations dans l'ordre lexicographique, ensuite, former le texte transformé à partir de la dernière lettre de chaque rotation.

##### Commençons par effecteur le tri des rotations :

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ c
/* compare_rotations : compare text rotations */
int compare_rotations(const char *t, int n, int fst, int snd)
{
        int i;
        for (i = 0; i < n && t[(fst+i) % n] == t[(snd+i) % n]; ++i) {
        }

        return t[(fst+i) % n] - t[(snd+i) % n];
}

/* sort : quick sort for Burrows-Wheeler rotations */
int sort(int *r, int n, const char *t, int t_size)
{
	if (n > 1) {
                int pivot = 0;

                for (int i = 1; i < n; i++) {
                        if (compare_rotations(t, t_size, r[i], r[pivot]) < 0)
                                pivot = swap(r, pivot, i);
                }

                sort(&r[0], pivot+1, t, t_size);
                sort(&r[pivot+1], (n-pivot)-1, t, t_size);
        }

	return 0;
}

/* swap : swap inline to int in t for quick sort */
int swap(int *t, int pivot, int index_to_move)
{
        int tmp = t[index_to_move];

        for(int i = index_to_move; i != pivot; --i)
                t[i] = t[i-1];

        t[pivot] = tmp;

        return pivot + 1;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour le tri, nous choisirons le [_quicksort ou tri rapide_](https://fr.wikipedia.org/wiki/Tri_rapide), il est rapide (\*really ?!\*) et récursif, ce qui est plus fun à implémenter, surtout dans le cadre d'un **tri en place**. A ce propos, le "tri en place" est une propriété d'un algorithme de tri, cela signifie que l'algorithme triera dans le tableau fourni en entrée, plutôt que de créer un tableau secondaire, placer les éléments trié à l'intérieur et retourner ce dernier (ce qui est plus simple en général).

Notre fonction de tri est donc la suivante `int sort(int *r, int n, const char *t, int t_size)`. Les paramètres sont alors un tableau contenant les rotations, c'est ce tableau qui doit être trié. Chaque case du tableau contient un entier, cet entier représente la rotation. Par exemple, si l'entier en question est 3, dans ce cas il représente le texte commençant par la 4ème lettre du texte d'origine. Les première, deuxième et troisième lettres se retrouvant à la fin de la rotation. Le second paramètre, `n`, représente la taille du tableau à trier. De plus, la chaîne de caractère à transformer fait également partie des paramètres (ainsi que sa taille, toujours) afin de pouvoir comparer les différentes rotations.

Comme pour toute fonction récursive, nous établissons une condition d'arrêt, ici ce sera `n <= 1`, cela afin d'éviter toute récursion infinie. Nous définissons alors le pivot, puis nous procédons à la recherche d'un élément situé après le pivot dans le tableau, mais qui devrait se trouver avant (boucle `for`). La comparaison ici est particulière, en effet, elle fait appel à une fonction spécifique car nous ne nous contentons pas de comparer deux entier un à un, mais deux rotations d'une même chaîne de caractère, ce qui complique un peu les choses ! Une fois qu'une rotation inférieure à la rotation du pivot est trouvée, nous plaçons cette rotation juste avant le pivot et décalons toutes les rotations d'un index (c'est le but de la fonction `swap`).

Nous pouvons alors effectuer deux appels récursifs, l'un avec un pointeur vers le tableau et la taille du début du tableau jusqu'au pivot, et l'autre avec un pointeur vers l'élément `pivot+1` du tableau et la taille restante. Ainsi, à chaque appel récursif le tableau est divisé en deux parties au niveau du pivot, sur chacune de ces parties, nous effectuons un tri, cela jusqu'à ce que nous ayons un tableau possédant 1 seule case. Dans ce cas, l'appel à la fonction retourne sans effectuer auparavant un appel récursif.

##### Le tri étant effectué, nous pouvons passer à la transformation !

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ c
/* bwtransform : operate the Burrows-Wheeler transform of the string t */
char *bwtransform(const char *t, int n)
{
	char *final = malloc((n + 1) * sizeof(char)); /* n+1 for trailing key */

	int *r = malloc(n * sizeof(int));
	for(int i = 0; i < n; ++i) {
		r[i] = i;
	}

	sort(r, n, t, n);

	for(int i = 0; i < n; ++i) {
		int index = (r[i] + (n - 1)) % n;
		final[i]= t[index];

		if (r[i] == 1)
			final[n] = i;
	}

	free(r);

	return final;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour la transformation, nous commençons par demander le tri des rotations puis nous bouclons sur chaque lettre du texte. Pour chacune de ces lettres, nous récupérons la dernière lettre de la rotation correspondante. Pour cela il nous faut `r[i]` correspondant à la rotation, nous lui ajoutons `(n-1)` afin d'obtenir la dernière lettre. Mais cela ne suffit pas, en effet, si nous partons de la 5è rotation (pour un texte de 10 lettres), dans ce cas `index = 5 + (10 - 1) =  14`, ce qui se trouve être en dehors de la chaîne. Un petit coup de modulo **taille_du_texte** devrait suffire à remettre les choses dans l'ordre. Au passage, lorsque `r[i] == 1`, nous en profitons pour mettre son index à la fin du texte comme étant la clé de la rotation (effectivement, lorsque l'on effectue une rotation de 1 caractère, la première lettre se retrouve être la dernière !). On peut enfin retourner le texte transformé.

## Transformée de Burrows-Wheeler, l'inverse

C'est beau d'avoir un texte transformé ! Par contre, si on ne peut pas récupérer le texte d'origine, ça perd vite de sa superbe...

Une fois n'est pas coutume, décrivons les étapes nécessaires à ce travail : tout d'abord il nous faut trier les lettres du texte transformé par ordre lexicographique croissant (*ouais, l'ordre alphabétique quoi*), une fois cela effectué nous pourrons associer à chaque lettre triée, la lettre correspondante dans le texte transformé, enfin il ne nous restera plus qu'a reconstituer le texte d'origine.

Pour trier les lettres, en toute sincérité, nous n'allons pas nous embêter, plutôt que d'effectuer une fois de plus un tri à proprement parler, nous nous contenterons d'effectuer le comptage de chaque lettre. Nous commençons donc par calculer le nombre d'occurrences de chaque lettre dans le texte transformé, et une fois cela effectué, il ne nous reste plus qu'à créer un second tableau de la taille du texte transformé, puis d'insérer successivement chaque lettre dans l'ordre.

##### Le tri du texte

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ c
/* frequencies : count frequency of each letter in t */
int *frequencies(const char *t, int n)
{
	int *freq = malloc(256 * sizeof(int));
	memset(freq, 0, 256 * sizeof(int));

	for(int i = 0; i < n; ++i) {
		int val = (int)t[i];
		++freq[val];
	}

	return freq;
}

/* sortchar : return a sorted copy of t */
char *sortchar(const char *t, int n)
{
	int *freq = frequencies(t, n);

	char *sorted_t = malloc(n * sizeof(char));
	int index = 0;

	for(int i = 0; i < 256; ++i) {
		int occur = freq[i];

		for(int j = 0; j< occur; ++j) {
			sorted_t[index] = i;
			++index;
		}
	}

	free(freq);

	return sorted_t;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Par exemple : pour le mot `adereda`, on compte le nombre d'occurrences de chaque lettre. On obtient alors `a : 2, z : 2, d : 2, r : 1`. Un fois cela effectué, il nous suffit de boucler dans les occurrences pour placer les dans le tableau trier les éléments, tout d'abord les 'a' (2 pour nous), puis les 'b' (aucun dans notre cas)... Ainsi le texte sera trié ! C'est une approche un peu naïve, mais malgré tout efficace, si l'on reprend le quicksort précédent, le tri se fait (en moyenne) avec une complexité de Onlog(n). Ici, la complexité est de O2n car nous effectuons deux parcours d'un tableau de taille n.

##### Associons maintenant chaque lettre triée à la lettre correspondante dans le texte transformé

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ c
/* find : find c in t[] */
static int find(char c, char *t, int n)
{
        for(int i = 0; i < n; ++i) {
                if (t[i] == c) {
                        t[i] = (char)0;
                        return i;
                }
        }

        return -1;
}

/* find_indexes : find index of sorted_t[i] in t */
int *find_indexes(const char *t, int n)
{
        char t_keyless[n-1];
        strncpy(t_keyless, t, n-1);

        char *sorted_t = sortchar(t_keyless, n-1);
        int *indexes = malloc((n-1) * sizeof(int));

        for(int i = 0; i < n-1; ++i)
                indexes[i] = find(sorted_t[i], t_keyless, n-1);

        free(sorted_t);
        return indexes;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour ce faire, nous récupérons le tableau trié, c'est un début. Nous créons ensuite un tableau `indexes` tel que `indexes[i]` se trouve être l'index de la lettre `sorted_t[i]` dans le texte transformé.
Pour repérer cette lettre, il nous suffit de parcourir le tableau (c'est le but de la fonction `static int find(char c, char *t, int n)`), une fois que nous avons trouvé la lettre recherchée, il nous suffit de retourner son index après l'avoir supprimée du tableau (d'où l'intérêt de `t[i] = (char)0;`) afin de ne pas retourner son index une seconde fois !

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ c
/* bwreverse : operate the Burrows-Wheeler reverse transform of the string t */
char *bwreverse(const char *t, int n)
{
        char *t_final = malloc((n-1) * sizeof(char));

        int *indexes = find_indexes(t, n);

        for(int i = 0, index = t[n-1]; i < n-1; ++i) {
                t_final[i] = t[index];
                index = indexes[index];
        }

        free(indexes);
        return t_final;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Enfin, l'ultime fonction, celle qui change le texte transformé en un texte lisible. Rien de bien compliqué une fois encore, nous nous contentons de créer un tableau qui fera office de texte lisible, et nous récupérons le tableau contenant les indexes. Pour cette dernière boucle `for`, the last one, nous déclarons deux compteurs, le premier pour parcourir les lettres de la chaîne finale les unes après les autres, et le second, qui aura comme valeur initiale `t[n-1]`, soit la clé de la transformation, et qui, à chaque fin de boucle, prendra comme valeur l'index de la lettre suivante dans le texte transformé grâce au tableau d'indexes crée juste avant !

---  

Note de bas de page: à vrai dire, l'accès à la `dernière case + 1` d'un tableau est toléré et ne cause pas d'overflow, ce qui est garanti par le Standard

Cet exercice est issu du sujet de concours de 2007 d'informatique de l'école Polytechnique.

Tout ce pavé est sujet à erreurs, n'hésitez pas à me le faire remarquer. Pour toute suggestion, remarque, question, précision, information, remerciement, beignet, prix Turing, c'est ici : **naccyde at naccyde dot eu**
